//
//  IntroScreenViewController.swift
//  Slanguage
//
//  Created by Alvaro Hare on 4/23/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit

class IntroScreenViewController: UIViewController {

    @IBOutlet weak var FirstBulletPoint: UIView!
    
    @IBOutlet weak var SecondBulletPoint: UIView!
    
    @IBOutlet weak var ThirdBulletPoint: UIView!
    
    @IBOutlet weak var FourthBulletPoint: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FirstBulletPoint.makeRoundedSquare()
        SecondBulletPoint.makeRoundedSquare()
        ThirdBulletPoint.makeRoundedSquare()
        FourthBulletPoint.makeRoundedSquare()
        // Do any additional setup after loading the view.
    }
    @IBAction func letsPlayPressed(_ sender: Any) {
        let navController = self.storyboard?.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
        UIApplication.shared.windows.first!.rootViewController = navController 
    }
}
