//
//  SlangWordViewController.swift
//  Slanguage
//
//  Created by Alvaro Hare on 2/25/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class SlangWordViewController: UIViewController, UITextViewDelegate{
    @IBOutlet weak var roundLabel: UILabel!
    
    @IBOutlet weak var charsLeftLabel: UILabel!
    @IBOutlet weak var slangLabel: UILabel!
    @IBOutlet weak var answerTextBox: UITextView!
    @IBOutlet weak var cardView: UIView!
    var initialClear = false
    var ref: DatabaseReference!
    var db = Firestore.firestore()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setRoundLabel()
        roundLabel.alpha = 0
        cardView.makeRoundedSquare()
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
        db = Firestore.firestore()
        loadRound()
        answerTextBox.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        fadeLabelIn()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        charsLeftLabel.text = "\(80 - answerTextBox.text.count)"
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return answerTextBox.text.count + (text.count - range.length) <= 80
    }
        
    func textViewDidBeginEditing(_ textView: UITextView) {
        if !initialClear {
            answerTextBox.text = String()
            initialClear = true
        }
    }
    
    func getRoundsFromDatabase(){
        
    }
    @IBAction func donePressed(_ sender: Any) {
        print("here")
        storeAnswer()
    }
    
    func goToWaiting(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WaitingViewController")
        
        guard let window = UIApplication.shared.keyWindow else {
            return
        }

        window.rootViewController = vc

       let options: UIView.AnimationOptions = .transitionFlipFromLeft

       let duration: TimeInterval = 0.8

       UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
       { completed in
           // maybe do something on completion here
       })
}
    
    func storeAnswer(){
        let roundText = "round" + String(game.roundNumber)
        print("here2")
        print(game.joinCode)
        db.collection("games").document(game.joinCode).collection("rounds").document(roundText).collection("answers").document(game.playerCode).setData(["playerCode": game.playerCode, "playerName": game.displayName, "answer": answerTextBox.text!, "hasVoted": false, "voters": []])
    }
    
    func fadeLabelIn(){
        UIView.animate(withDuration: 1, animations: {
            self.roundLabel.alpha = 1
        }, completion: {
            (value: Bool) in
            sleep(1)
            self.fadeLabelOut()
        })
    }
    
    func fadeLabelOut(){
        UIView.animate(withDuration: 1) {
            self.roundLabel.alpha = 0
        }
    }
    
    func loadRound(){
        let numRoundString = "round" + String(game.roundNumber)
        print("currentRound")
        print(numRoundString)
        print(game.rounds.count)
        slangLabel.text = game.rounds[game.roundNumber-1].slang
    }
    
    func setRoundLabel(){
        roundLabel.text = "Round " + String(game.roundNumber)
    }
}
