//
//  GameObject.swift
//  Slanguage
//
//  Created by Alvaro Hare on 2/18/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit

class GameObject{
    var isCreator = false
    var joinCode: String
    var creator: String
    var displayName: String
    var playerCode: String
    var numRounds = 0
    var roundNumber = 1
    var numPlayers = 1
    var loadedRounds = [Round]()
    var rounds = [Round]()
    var roundWinners = [String]()
    var winnerDefs = [String]()
    var roundWinnerCodes = [String]()
    var isVoting = false
    var isInRoundWinner = false
    
    init(){
        self.displayName = ""
        self.creator = ""
        self.joinCode = ""
        self.playerCode = ""
    }
    init(displayName: String, creator: String, joinCode: String, playerCode: String){
        self.playerCode = playerCode
        self.displayName = displayName
        self.creator = creator
        self.joinCode = joinCode
    }

}

extension String {

    static func random(length: Int = 6) -> String {
        let base = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        var randomString: String = ""

        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
}
