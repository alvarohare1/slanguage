//
//  AllAnswersViewController.swift
//  Slanguage
//
//  Created by Alvaro Hare on 2/28/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

struct Answer{
    var text: String
    
    init(text: String) {
        self.text = text
    }
}

class AllAnswersViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var v = [String]()
    var vote = false
    var answers = [Answer]()
    var playersInOrder = [String]()
    let roundText = "round" + String(game.roundNumber)
    var winners = [String]()

    var votedRow = 0

    @IBOutlet weak var slangLabel: UILabel!
    
    @IBAction func voteButton(_ sender: Any) {
        vote = true
        //tableView.reloadData()
        let buttonPosition = (sender as AnyObject).convert(CGPoint(), to:tableView)
        let indexPath = tableView.indexPathForRow(at:buttonPosition)
        
        let playerVotedFor = playersInOrder[indexPath!.row]
        votedRow = indexPath!.row
        //print(playerVotedFor)
        
        //let roundText = "round" + String(game.roundNumber)
        let db = Firestore.firestore()
        db.collection("games").document(game.joinCode).collection("rounds").document(roundText).collection("answers").document(playerVotedFor).updateData(["voters": FieldValue.arrayUnion([game.playerCode])])
        
        db.collection("games").document(game.joinCode).collection("rounds").document(roundText).collection("answers").document(game.playerCode).updateData(["hasVoted": true])
        
        
        tableView.reloadData()
        checkVotes()

    }
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        slangLabel.layer.masksToBounds = true
        slangLabel.makeRoundedSquare()
        slangLabel.text = game.rounds[game.roundNumber-1].slang
        game.isInRoundWinner = false
        getAnswers()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "VotingCell", for: indexPath as IndexPath) as! VotingTableViewCell
        
        let row = indexPath.row
        cell.answerTextField?.text = answers[row].text
        cell.answerTextField.isUserInteractionEnabled = false
        if vote == true{
            cell.voteButton.isHidden = true
            if votedRow == row{
                cell.backgroundColor = UIColor(red: 0.97, green: 0.40, blue: 0.40, alpha: 1.00)
                cell.backgroundColor?.withAlphaComponent(0.35)
            }
    
        }
    
        return cell
    }
    
    func checkVotes(){
        var playersVoted = [Bool]()
        let db = Firestore.firestore()
        


        db.collection("games").document(game.joinCode).collection("rounds").document(self.roundText).collection("answers").addSnapshotListener(){
        (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {                db.collection("games").document(game.joinCode).collection("rounds").document(self.roundText).collection("answers").getDocuments(){
            (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    var hasVoted = [Bool]()
                    for document in querySnapshot!.documents {
                        let checkIfVoted = document.get("hasVoted") as! Bool
                        
                        hasVoted.append(checkIfVoted)
                        playersVoted = hasVoted
                    }
                    if !playersVoted.contains(false) && playersVoted.count == game.numPlayers && !game.isInRoundWinner{
                        self.getWinner()
                        db.collection("games").document(game.joinCode).collection("rounds").document(self.roundText).updateData(["allVoted": true])
                    }
                    print(playersVoted)
                }
           
                }
            }
       }
    }
    
    
    
    func getWinner(){
        game.isInRoundWinner = true
        game.roundWinners = []
        game.winnerDefs = []
        //var counted = [Bool](repeating: false, count: answers.count)
        var count = [Int]()
        let db = Firestore.firestore()
        db.collection("games").document(game.joinCode).collection("rounds").document(self.roundText).collection("answers").getDocuments(){
         (querySnapshot, err) in
             if let err = err {
                 print("Error getting documents: \(err)")
             } else {
                var rw = [String]()
                var rwCodes = [String]()
                var ans = [String]()
                 var cnt = 0
                 //var countedIdx = 0
                 for document in querySnapshot!.documents {
                    let voteCounts = document.get("voters") as! Array<Any>
                    let name = document.get("playerName") as! String
                    let id = document.get("playerCode") as! String
                    let answer = document.get("answer") as! String
                    
                    if voteCounts.count > cnt{
                        cnt = voteCounts.count
                        rw = [name]
                        rwCodes = [id]
                        ans = [answer]
                    }
                    else if voteCounts.count > 0 && voteCounts.count == cnt{
                        rw.append(name)
                        rwCodes.append(id)
                        ans.append(answer)
                    }
                    self.winners = rw
                    game.roundWinners = self.winners
                    game.roundWinnerCodes = rwCodes
                    game.winnerDefs = ans
                 }
                self.updateTotalVotes()
               
                guard let window = UIApplication.shared.keyWindow else {
                   return
                }
               
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "RoundWinnerView")

                window.rootViewController = vc

                let options: UIView.AnimationOptions = .transitionFlipFromLeft

                let duration: TimeInterval = 0.8

                UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
                { completed in
                   // maybe do something on completion here
                })
                    
             
                
            }
           
        }
    }
    
    
    func updateTotalVotes(){
        let db = Firestore.firestore()
        for code in game.roundWinnerCodes {
            db.collection("games").document(game.joinCode).collection("players").document(code).updateData(["roundsWon": FieldValue.arrayUnion([game.roundNumber])])
        }
    }
    
    func getAnswers(){
        print("getAnswers")
        
        let db = Firestore.firestore()
        let roundText = "round" + String(game.roundNumber)
       
        db.collection("games").document(game.joinCode).collection("rounds").document(roundText).collection("answers").addSnapshotListener { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                db.collection("games").document(game.joinCode).collection("rounds").document(roundText).collection("answers").getDocuments(){
            (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    var ans = [Answer]()
                    var playersOrd = [String]()
                    for document in querySnapshot!.documents {
                        let playerAns = document.get("answer") as! String
                        let playerCode = document.get("playerCode") as! String
                        
                        playersOrd.append(playerCode)
                        ans.append(Answer(text: playerAns))
                        self.answers = ans
                        self.playersInOrder = playersOrd
                        self.tableView.reloadData()
                        
                    }
                  
                }
           
                }
                
            }
        }
        
    }
}
