//
//  ChooseGameViewController.swift
//  Slanguage
//
//  Created by Alvaro Hare on 2/13/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit
import FirebaseDatabase
var game = GameObject()

class ChooseGameViewController: UIViewController {
    
    let defaults = UserDefaults.standard

    
    override func viewDidLoad() {
        super.viewDidLoad()
        if defaults.bool(forKey: "Has Launched") == true {
            
            print("Second+")
            defaults.set(true, forKey: "Has Launched")
            
        } else {
            
            print("First")
            performSegue(withIdentifier: "HowToPlay", sender: nil)
            defaults.set(true, forKey: "Has Launched")
        }
        
        

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    

}
