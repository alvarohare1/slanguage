//
//  CreateGameViewController.swift
//  Slanguage
//
//  Created by Alvaro Hare on 2/13/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore



class Round {
    var slang: String
    var definition: String
    
    init(slang: String, definition: String) {
        self.slang = slang
        self.definition = definition
    }
}

public var ids = [String]()

class CreateGameViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var charCounter: UILabel!
    @IBOutlet weak var newGameCode: UILabel!
    @IBOutlet weak var displayName: UITextField!
    var ref: DatabaseReference!
    var db = Firestore.firestore()
    
    @IBAction func backButtonPressed(_ sender: Any) {
        game = GameObject()
        let navController = self.storyboard?.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
        UIApplication.shared.windows.first!.rootViewController = navController 
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        displayName.delegate = self
        game.isCreator = true
        newGameCode.text = String.random()
        game.joinCode = newGameCode.text!
        displayName.text = ""
        db = Firestore.firestore()
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength = textField.text!.count + string.count - range.length

        if 12 - newLength >= 0 {
            charCounter.text = String(12 - newLength)
        }
        return newLength <= 12
    }

    @IBAction func createGamePressed(_ sender: Any){
        if displayName.text != "" {
            let db = Firestore.firestore()
            let playerID = String.random()
            game.playerCode = playerID
            game.displayName = displayName.text!
            setCreator()
        }
        else {
            let confirmationAlert = UIAlertController(title: "Display Name is empty", message: "Please choose a unique name to play under!", preferredStyle: .alert)
            confirmationAlert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
            self.present(confirmationAlert, animated: true)
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "createGameSegue" {

            let nextVC = segue.destination as? GameLobbyViewController
            nextVC!.joinCode = newGameCode.text!
            nextVC!.isCreator = true
        }
    }
    
    func setCreator(){
        
        ids.append(game.playerCode)
        
        db.collection("games").document(newGameCode.text!).setData(["creator": game.playerCode, "started":false])
        db.collection("games").document(game.joinCode).collection("players").document(game.playerCode).setData(["name": displayName.text!, "roundsWon": []])
    }
    
}
