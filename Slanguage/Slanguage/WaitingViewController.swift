//
//  WaitingViewController.swift
//  Slanguage
//
//  Created by Ryan Gutierrez on 2/28/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit
import Foundation
import Firebase
import FirebaseFirestore

class WaitingViewController: UIViewController {
    //@IBOutlet weak var card1: UIView!
    //@IBOutlet weak var card2: UIView!
    //@IBOutlet weak var card3: UIView!
    //@IBOutlet weak var card4: UIView!
    //@IBOutlet weak var card5: UIView!
    //@IBOutlet weak var card6: UIView!
    //@IBOutlet weak var card7: UIView!
    //@IBOutlet weak var card8: UIView!
    
    
    //@IBOutlet weak var frontCard: UIView!
    //@IBOutlet weak var backCard: UIView!
    //@IBOutlet var frontCard: UIView!
    
    //@IBOutlet var backCard: UIView!
    var allPlayersReady = false
    //@IBOutlet weak var dotsLabel: UILabel!
    let roundText = "round" + String(game.roundNumber)
    var answers = [Answer]()
        var playersInOrder = [String]()
    
    var isFlipped = false
    //let containerView = UIView()
    
    var frontCard:UIView!
    var backCard:UIView!
    
    var containerView:UIView!
    
  
    
    var logoImg = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let width = self.view.frame.width * 0.8
        let height = self.view.frame.height * 0.55
        
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width * 0.9, height: height * 0.5))
        //label.center = CGPoint(x: 160, y: 285)
        label.textAlignment = .center
        label.text = "waiting for the other players to finish..."
        label.font = UIFont(name: "JosefinSans-Bold", size: 30.0)
        label.textColor = UIColor.white
        label.numberOfLines = 3
        label.lineBreakMode = .byWordWrapping
        
        
        frontCard = UIView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        backCard = UIView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        containerView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        let logoImg = UIImageView(frame: CGRect(x: 0, y: 0, width: width * 0.95, height: height * 0.65))

    
        frontCard.backgroundColor = UIColor(red: 0.97, green: 0.40, blue: 0.40, alpha: 1.00)
        backCard.backgroundColor = UIColor(red: 0.03, green: 0.53, blue: 0.59, alpha: 1.00)
        
        logoImg.image = UIImage(named: "logoCards.png")
        
        
       
    
        
        frontCard.addSubview(label)
        backCard.addSubview(logoImg)
        
        containerView.addSubview(frontCard)
        logoImg.center = containerView.center
        label.center = frontCard.center
        //containerView.addSubview(backCard)
        roundCorners(card: frontCard)
        roundCorners(card: backCard)
        
        view.addSubview(containerView)
        
        //containerView.addSubview(frontCard)
        //containerView.addSubview(backCard)
        game.isVoting = false
        //let db = Firestore.firestore()
       
        
       
        
        var timer = Timer.scheduledTimer(timeInterval: 2.0,
        target: self,
        selector: #selector(self.flipCard),
        userInfo: nil,
        repeats: true)
        /*roundCorners(card: card2)
        roundCorners(card: card3)
        roundCorners(card: card4)
        roundCorners(card: card5)
        roundCorners(card: card6)
        roundCorners(card: card7)
        roundCorners(card: card8)*/
        print("here4")
        //checkAnswers()
        getAnswers()
    }
    
    @objc func flipCard(){
        
        var showingSide = frontCard
        var hiddenSide = backCard
        if isFlipped {
            (showingSide, hiddenSide) = (backCard, frontCard)
        }
        
        UIView.transition(from: showingSide!, to: hiddenSide!, duration: 1, options: UIView.AnimationOptions.transitionFlipFromRight,
        completion: nil)
            
        
        isFlipped = !isFlipped
    }
    
    override func viewWillLayoutSubviews() {
        containerView.center = view.center
        //logoImg.center = containerView.center
    }

    func roundCorners(card: UIView){
        card.layer.cornerRadius = 15;
        card.layer.masksToBounds = true;
        
        
    }
    
    func checkAnswers(){
       let db = Firestore.firestore()
       db.collection("games").document(game.joinCode).collection("rounds").document(self.roundText).collection("answers").getDocuments(){
        (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                var numAnswers = 0
                for document in querySnapshot!.documents {
                    numAnswers += 1
                }
                if numAnswers == game.numPlayers{
                    self.goToVoting()
                }
            }
        }
    }
    
    func goToVoting(){
        
        game.isVoting = true
        
        guard let window = UIApplication.shared.keyWindow else {
           return
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VotingViewController")

        window.rootViewController = vc

        let options: UIView.AnimationOptions = .transitionFlipFromLeft

        let duration: TimeInterval = 0.8

        UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
        { completed in
           // maybe do something on completion here
        })
    }
    
    func getAnswers(){
        print("getAnswers")
        
        let db = Firestore.firestore()
        let roundText = "round" + String(game.roundNumber)
        db.collection("games").document(game.joinCode).collection("rounds").document(roundText).collection("answers").addSnapshotListener { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                db.collection("games").document(game.joinCode).collection("rounds").document(roundText).collection("answers").getDocuments(){
            (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    var ans = [Answer]()
                    var playersOrd = [String]()
                    for document in querySnapshot!.documents {
                        let playerAns = document.get("answer") as! String
                        let playerCode = document.get("playerCode") as! String
                        
                        playersOrd.append(playerCode)
                        ans.append(Answer(text: playerAns))
                        self.answers = ans
                        self.playersInOrder = playersOrd
                    }
                    if self.answers.count == game.numPlayers {
                        if !game.isVoting {
                            self.goToVoting()
                            db.collection("games").document(game.joinCode).collection("rounds").document(self.roundText).updateData(["allAnswered": true])
                        }
                    }
                  
                }
           
            }
        
            }
        }
        
    }
}
