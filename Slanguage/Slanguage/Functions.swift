//
//  Functions.swift
//  Slanguage
//
//  Created by Alvaro Hare on 2/27/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//
import UIKit
import Foundation

extension UIView {
    func makeRoundedSquare() {
        self.layer.cornerRadius = 15.0
    }
    
    func makeRoundedLine() {
        self.layer.cornerRadius = 5.0
    }
}
