//
//  JoinGameViewController.swift
//  Slanguage
//
//  Created by Alvaro Hare on 2/13/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class JoinGameViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var joinCode: UITextField!
    @IBOutlet weak var displayName: UITextField!
    @IBOutlet weak var charCounter: UILabel!
    var rounds = [Round]()
    
    @IBAction func backButtonPressed(_ sender: Any) {
        game = GameObject()
        let navController = self.storyboard?.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
        UIApplication.shared.windows.first!.rootViewController = navController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        displayName.delegate = self
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
        game.isCreator = false
        joinCode.autocapitalizationType = .allCharacters
        // Do any additional setup after loading the view.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength = textField.text!.count + string.count - range.length
        print("here")
        if 12 - newLength >= 0 {
            charCounter.text = String(12 - newLength)
        }
        return newLength <= 12
    }
    
    @IBAction func joinButton(_ sender: Any) {
        if joinCode.text != "" {
            let db = Firestore.firestore()
            let joinText = joinCode.text!
            game.displayName = displayName.text!
            game.joinCode = joinText
            print(game.joinCode)
            db.collection("games").document(joinText).getDocument { (document, error) in
            if let document = document, document.exists {
                    let playerID = String.random()
                    game.playerCode = playerID
                    if self.displayName.text != "" {
                        db.collection("games").document(joinText).collection("players").document (playerID).setData(["name": self.displayName.text!, "roundsWon": []])
                    }
                    else {
                        let confirmationAlert = UIAlertController(title: "Display Name is empty", message: "Please choose a unique name to play under!", preferredStyle: .alert)
                        confirmationAlert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                        self.present(confirmationAlert, animated: true)
                    }
                    //print(id)
                    print("code exists")

                }else{
                     let confirmationAlert = UIAlertController(title: "Not a valid code", message: "Please get a valid code from the game creator", preferredStyle: .alert)
                     confirmationAlert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                     self.present(confirmationAlert, animated: true)
                    
                }
            }
        }
        else{
            let confirmationAlert = UIAlertController(title: "No code entered!", message: "Please get a valid code from the game creator", preferredStyle: .alert)
            confirmationAlert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
            self.present(confirmationAlert, animated: true)
        }

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "joinGameSegue" {

            let nextVC = segue.destination as? GameLobbyViewController
            nextVC!.joinCode = joinCode.text!
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "joinGameSegue" {
            if displayName.text == "" || joinCode.text == "" {
                return false
            }
        }
        return true
    }
}
