//
//  RoundWinnerViewController.swift
//  Slanguage
//
//  Created by Ryan Gutierrez on 2/28/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit

class RoundWinnerViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    

    @IBOutlet weak var redCard: UIView!
    @IBOutlet weak var blueCard: UIView!
    @IBOutlet weak var actualDef: UILabel!
    //@IBOutlet weak var winnerNames: UILabel!
    @IBOutlet weak var finishButton: UIButton!
    @IBOutlet weak var slangWord: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var winnerNames = [String]()
    var roundDefs = [String]()
    
    @IBOutlet weak var roundWinnerLabel: UILabel!
    //@IBOutlet weak var actualMeaning: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(game.winnerDefs)
        //actualMeaning.adjustsFontSizeToFitWidth = true
        
        if game.roundWinners.count > 1{
            roundWinnerLabel.text = "ROUND WINNERS"
        }
        slangWord.text = game.rounds[game.roundNumber-1].slang
        actualDef.sizeToFit()
        actualDef.numberOfLines = 0
        actualDef.adjustsFontSizeToFitWidth = true
        
        roundWinnerLabel.adjustsFontSizeToFitWidth = true
       
        
       // winnerNames.numberOfLines = 0
        slangWord.layer.masksToBounds = true
        slangWord.makeRoundedSquare()
        slangWord.sizeToFit()
        slangWord.adjustsFontSizeToFitWidth = true
        //print(game.roundWinners)
        winnerNames = game.roundWinners
        roundDefs = game.winnerDefs
       /* if game.roundWinners.count > 1{
            winnerNames.text = game.roundWinners.joined(separator: ", ")
        }
        else{
            winnerNames.text = game.roundWinners[0]
        }*/
        
        game.roundWinners = [String]()
        game.roundWinnerCodes = [String]()
        game.winnerDefs = [String]()
        
        roundCorners(card: redCard)
        roundCorners(card: blueCard)
        actualDef.text = game.rounds[game.roundNumber - 1].definition
        print("roundWinnerVC")
        print(game.roundNumber)
        print(game.numRounds)
        
        if game.roundNumber > game.numRounds {
            finishButton.setTitle("FINISH", for: .normal)
            finishButton.titleLabel?.adjustsFontSizeToFitWidth = true
        }
        else {
            finishButton.setTitle("NEXT ROUND", for: .normal)
            finishButton.titleLabel?.adjustsFontSizeToFitWidth = true
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return winnerNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WinnerCell", for: indexPath as IndexPath) as! RoundWinnerCell
        
        print("cellforrowat")
        let row = indexPath.row
        cell.playerName?.text = winnerNames[row]
        cell.playerName.sizeToFit()
        cell.playerName.adjustsFontSizeToFitWidth = true
        
        cell.playerName.layer.cornerRadius = 7;
        cell.playerName.layer.masksToBounds = true;
        
        cell.playerDef?.text = roundDefs[row]
        //cell.playerDef.sizeToFit()
        cell.playerDef.numberOfLines = 0
        cell.playerDef.adjustsFontSizeToFitWidth = true
        print(winnerNames)
          
        return cell
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func finishPressed(_ sender: Any) {
        var view = ""
        
        if game.roundNumber > game.numRounds {
            print("ending game")
            view = "EndView"
        }
        else {
            print("going back to game")
            view = "GameNavigationController"
            game.roundNumber = game.roundNumber + 1
        }
        
        print(view)
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        game.roundWinners = [String]()
        game.roundWinnerCodes = [String]()
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: view)

        window.rootViewController = vc

        let options: UIView.AnimationOptions = .transitionFlipFromLeft

        let duration: TimeInterval = 0.8

        UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
        { completed in
            // maybe do something on completion here
        })
    }
    
    func roundCorners(card: UIView){
        card.layer.cornerRadius = 10;
        card.layer.masksToBounds = false;
    }
}
