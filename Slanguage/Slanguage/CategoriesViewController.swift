//
//  CategoriesViewController.swift
//  Slanguage
//
//  Created by Ryan Gutierrez on 2/27/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class CategoriesViewController: UIViewController {

    @IBOutlet weak var card4: UIView!
    @IBOutlet weak var card5: UIView!
    @IBOutlet weak var card3: UIView!
    @IBOutlet weak var card2: UIView!
    @IBOutlet weak var card1: UIView!
    
    @IBOutlet weak var ninetiesDescript: UILabel!
    @IBOutlet weak var thousandsDescript: UILabel!
    
    var ref: DatabaseReference!
    var db = Firestore.firestore()
    var rounds = [Round]()
    
    
    @IBAction func nineties(_ sender: Any) {
        self.view.bringSubviewToFront(card4)
        ninetiesDescript.numberOfLines = 0
    }
    @IBAction func BACK(_ sender: Any) {
        self.view.bringSubviewToFront(card5)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        roundCorners(card: card1)
        roundCorners(card: card2)
        roundCorners(card: card3)
        roundCorners(card: card4)
        roundCorners(card: card5)
        self.view.bringSubviewToFront(card5)
        thousandsDescript.numberOfLines = 0
        // Do any additional setup after loading the view.
    }
    
    func roundCorners(card: UIView){
        card.layer.cornerRadius = 10;
        card.layer.masksToBounds = false;
        
        card.layer.shadowPath = UIBezierPath(rect: card.bounds).cgPath
        card.layer.shadowRadius = 5
        card.layer.shadowOffset = .zero
        card.layer.shadowOpacity = 1
    }
}
