//
//  PopoverViewController.swift
//  Slanguage
//
//  Created by Alvaro Hare on 4/21/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit

class PopoverViewController: UIViewController {

    @IBOutlet weak var textField: UITextView!
    @IBOutlet weak var topRectagle: UIView!
    @IBOutlet weak var developersLabel: UILabel!
    @IBOutlet weak var contentCreatorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        developersLabel.layer.masksToBounds = true
        developersLabel.makeRoundedSquare()
        contentCreatorLabel.layer.masksToBounds = true
        contentCreatorLabel.makeRoundedSquare()
        topRectagle.makeRoundedLine()
    }
}
