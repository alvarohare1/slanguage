//
//  VotingTableViewCell.swift
//  Slanguage
//
//  Created by Alvaro Hare on 2/29/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit

class VotingTableViewCell: UITableViewCell {
    @IBOutlet weak var answerTextField: UITextView!
    @IBOutlet weak var voteButtonPressed: UIButton!
    @IBOutlet weak var voteButton: UIButton!
    
}
