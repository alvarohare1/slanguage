//
//  EndOfGameTableViewCell.swift
//  Slanguage
//
//  Created by Alvaro Hare on 2/28/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit

class EndOfGameTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var points: UILabel!

}
