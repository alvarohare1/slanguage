//
//  GameLobbyTableViewCell.swift
//  Slanguage
//
//  Created by Alvaro Hare on 2/20/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit

class GameLobbyTableViewCell: UITableViewCell {

    
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var readyLabel: UILabel!

}
