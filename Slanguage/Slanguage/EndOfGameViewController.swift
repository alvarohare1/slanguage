//
//  EndOfGameViewController.swift
//  Slanguage
//
//  Created by Alvaro Hare on 2/25/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

struct Player{
    
    var name: String = ""
    var points: Int = 0
    
    init(name: String, points: Int){
        self.name = name
        self.points = points
    }
}

import UIKit
import Firebase
import FirebaseFirestore

class EndOfGameViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var winnerText: UILabel!
    @IBOutlet weak var winnerPoints: UILabel!
    @IBOutlet weak var otherPlayersTableView: UITableView!
    @IBAction func homePressed(_ sender: Any) {
        game = GameObject()
        let navController = self.storyboard?.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
        UIApplication.shared.windows.first!.rootViewController = navController    }
    
    var winningScore = 0
    var winnersArray = [String]()
    var otherPlayersArray = [Player]()
    var ref: DatabaseReference!
    var db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        otherPlayersTableView.delegate = self
        otherPlayersTableView.dataSource = self
        db = Firestore.firestore()
        getPlayers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return otherPlayersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EndOfGameCell", for: indexPath as IndexPath) as! EndOfGameTableViewCell
        
        let row = indexPath.row
        cell.name?.text = otherPlayersArray[row].name
        cell.points?.text = String(otherPlayersArray[row].points)
        return cell
    }

    func getPlayers(){
        let db = Firestore.firestore()
        db.collection("games").document(game.joinCode).collection("players").addSnapshotListener { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                db.collection("games").document(game.joinCode).collection("players").getDocuments(){
            (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        let displayName = document.get("name") as! String
                        let roundsWon = document.get("roundsWon") as! Array<Any>
                        let newPlayer = Player(name: displayName, points: roundsWon.count)
                        
                        if roundsWon.count > self.winningScore {
                            self.winningScore = roundsWon.count
                        }
                        self.otherPlayersArray.append(newPlayer)
                        self.otherPlayersTableView.reloadData()
                    }
                    self.getWinners()
                }
           
                }
            }
                
            }
        }
    
    func getWinners()
    {
        for player in otherPlayersArray {
            if player.points == winningScore{
                winnersArray.append(player.name)
            }
        }
        
        if winnersArray.count > 1{
            winnerText.numberOfLines = winnersArray.count + 1
            winnerText.text = winnersArray.joined(separator: ", ") + " are the winners!"
            winnerPoints.text = String(winningScore)
        }
        else{
            winnerText.text = winnersArray[0] + " is the winner!"
            winnerPoints.text = String(winningScore)
        }
        
    }
}
