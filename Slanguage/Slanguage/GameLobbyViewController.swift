//
//  GameLobbyViewController.swift
//  Slanguage
//
//  Created by Alvaro Hare on 2/13/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore


class GameLobbyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
  
    var isCreator = false
    var playerNames = [String]()
    var joinCode = String()
    @IBOutlet weak var joinCodeLabel: UILabel!
    @IBOutlet weak var playersTableView: UITableView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var gameCodeLabel: UILabel!
    @IBOutlet weak var playersLabel: UILabel!
    var ref: DatabaseReference!
    var db = Firestore.firestore()
    
    @IBAction func backButtonPressed(_ sender: Any) {
        game = GameObject()
        let navController = self.storyboard?.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
        UIApplication.shared.windows.first!.rootViewController = navController 
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        getPlayers()
        gameCodeLabel.adjustsFontSizeToFitWidth = true;
        gameCodeLabel.sizeToFit()
        
        playersLabel.adjustsFontSizeToFitWidth = true;
        playersLabel.sizeToFit()
        
        
        joinCodeLabel.text = joinCode
        playersTableView.delegate = self
        playersTableView.dataSource = self
        if !isCreator {
            print("not creator")
            startButton.isHidden = true
            listenForStart()
        }
        db = Firestore.firestore()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func startPressed(_ sender: Any) {
        if isCreator{
            getRounds()
        }
        else{
            print("join start")
            db.collection("games").document(joinCode).collection("rounds").getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    print("getting rounds")
                    game.numRounds = 4
                    game.roundNumber = 1
                    print(querySnapshot!.documents)
                    for document in querySnapshot!.documents {
                        let slang = document.get("slang") as! String
                        let definition = document.get("definition") as! String
                        let round = Round(slang: slang, definition: definition)
                        game.rounds.append(round)
                        print("round appended")
                    }
                    guard let window = UIApplication.shared.keyWindow else {
                        return
                    }
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "GameNavigationController")

                    window.rootViewController = vc

                    let options: UIView.AnimationOptions = .transitionFlipFromLeft

                    let duration: TimeInterval = 0.8
                    
                    print("transitioning")
                    UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
                    { completed in
                        // maybe do something on completion here
                    })
                }
            }

        }
        game.numPlayers = playerNames.count

    }
    
    func listenForStart(){
        db.collection("games").document(joinCode)
        .addSnapshotListener { documentSnapshot, error in
          guard let document = documentSnapshot else {
            print("Error fetching document: \(error!)")
            return
          }
          guard let data = document.data() else {
            print("Document data was empty.")
            return
          }
            print("Current data: \(data)")
            print(data["started"]!)
            var started = data["started"]! as! Bool
            if started == true {
                self.startPressed(self)
            }
        }
    }
    
    func startGame() {
        db.collection("games").document(joinCode).updateData(["started": true])
        print("game started")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playerNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerLobbyCell", for: indexPath as IndexPath) as! GameLobbyTableViewCell
        let row = indexPath.row
        cell.playerName?.text = playerNames[row]
        cell.readyLabel?.text = "Ready"
        return cell
    }
    
    func getPlayers(){
        let db = Firestore.firestore()
        db.collection("games").document(joinCode).collection("players").addSnapshotListener { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                db.collection("games").document(self.joinCode).collection("players").getDocuments(){
            (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    var players = [String]()
                    for document in querySnapshot!.documents {
                        let displayName = document.get("name") as! String
                        
                        players.append(displayName)
                        self.playerNames = players
                        self.playersTableView.reloadData()
                    }
                }
           
                }
                
            }
        }
        
    }
    
    func getRounds(){
           db.collection("categories").document("2000").collection("terms").addSnapshotListener { (querySnapshot, err) in
               if let err = err {
                   print("Error getting documents: \(err)")
               } else {
                print("categories")
                   self.db.collection("categories").document("2000").collection("terms").getDocuments(){
               (querySnapshot, err) in
                   if let err = err {
                       print("Error getting documents: \(err)")
                   } else {
                    if game.loadedRounds.count != 0{
                        game.loadedRounds = []
                    }
                       for document in querySnapshot!.documents {
                           let slang = document.get("slang") as! String
                           let definition = document.get("definition") as! String
                           let round = Round(slang: slang, definition: definition)
                           //print(round)
                           print("adding rounds to game")
                           game.loadedRounds.append(round)
                           //print(slang)
                       }
                      // print(game.loadedRounds)
                       self.setRounds()

                        guard let window = UIApplication.shared.keyWindow else {
                            return
                        }
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GameNavigationController")

                        window.rootViewController = vc

                        let options: UIView.AnimationOptions = .transitionFlipFromLeft

                        let duration: TimeInterval = 0.8

                        UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
                        { completed in
                            // maybe do something on completion here
                        })
                    }
                   }
                   
               }
           }
           
           
       }
       
       func setRounds(){
            var numRound = 1
            game.numRounds = 4
            game.roundNumber = 1
            var currentRoundsLoaded = 0
            var indexesUsed = [Int]()
        while currentRoundsLoaded <= game.numRounds {
            var wordIndex = Int.random(in: 0 ..< game.loadedRounds.count)
            if indexesUsed.contains(wordIndex) {
                //try again
            }
            else {
                print(wordIndex)
                indexesUsed.append(wordIndex)
                currentRoundsLoaded += 1
                let round = game.loadedRounds[wordIndex]
                game.rounds.append(round)
                let roundText = "round" + String(numRound)
                db.collection("games").document(game.joinCode).collection("rounds").document(roundText).setData(["slang": round.slang, "definition": round.definition, "allVoted": false, "allAnswered": false])
                   numRound += 1
                    print("set round")
            }
            
        }
        startGame()
       }

}
