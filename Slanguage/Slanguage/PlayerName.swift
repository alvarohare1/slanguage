//
//  playerName.swift
//  Slanguage
//
//  Created by Ryan Gutierrez on 2/24/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import Foundation
import UIKit

class PlayerName{
    var name: String
    
    init(name: String){
        self.name = name
    }
    
}
