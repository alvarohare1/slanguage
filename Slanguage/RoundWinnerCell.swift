//
//  RoundWinnerCell.swift
//  Slanguage
//
//  Created by Ryan Gutierrez on 5/2/20.
//  Copyright © 2020 Alvaro Hare. All rights reserved.
//

import Foundation
import UIKit

class RoundWinnerCell: UITableViewCell{
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerDef: UILabel!
    
}
